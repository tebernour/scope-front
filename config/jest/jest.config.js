module.exports = {
  rootDir: '../../',
  setupFiles: ['<rootDir>/config/jest/jest.setup.js'],
  coverageThreshold: {
    global: {
      branches: 68,
      functions: 77,
      lines: 81,
      statements: 81,
    },
  },
  // setupFilesAfterEnv: ["<rootDir>/config/jest/jest.after_setup.js"],
};
