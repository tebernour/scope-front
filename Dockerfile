FROM node:10.19
LABEL maintainer="Amine gheribi <ma.gheribi@oyez.fr>" 
WORKDIR /app
COPY package.json package.json
RUN npm install
RUN cp .env.${ENV}.env && rm .env.*
EXPOSE 3000
RUN yarn build
CMD ["yarn", "start"]



