export const subjectList = [
  {
    name: 'Déclarer un PND / NPAI',
    value: 1,
    topic: 1,
  },
  {
    name: 'Fermer une relation véhicule supérieure à 4 mois',
    value: 2,
    topic: 1,
  },
  {
    name: 'Fermer une relation véhicule inférieure à 4 mois',
    value: 3,
    topic: 1,
  },
  {
    name: 'Stop COM tous canaux',
    value: 4,
    topic: 1,
  },
  {
    name: 'Stop COM courrier et/ou téléphone',
    value: 5,
    topic: 1,
  },
  {
    name: 'Stop COM email',
    value: 6,
    topic: 1,
  },
  {
    name: 'Stop COM SMS',
    value: 7,
    topic: 1,
  },
  {
    name: 'Modification adresse, email telephone',
    value: 8,
    topic: 1,
  },
  {
    name: 'Déclarer un client décédé',
    value: 9,
    topic: 1,
  },
  {
    name: 'Suppression email / téléphone',
    value: 10,
    topic: 1,
  },
  {
    name: 'Demande dans le cadre de la RGPD (droit à l\'oubli)',
    value: 11,
    topic: 1,
  },
  {
    name: 'Télécharger le guide utilisateur',
    value: 12,
    topic: 2,
  },
  {
    name: 'Télécharger le guide des supports Catalogue Renault Scope',
    value: 13,
    topic: 2,
  },
  {
    name: 'Gestion des profils utilisateurs',
    value: 14,
    topic: 2,
  },
  {
    name: 'Avancement  de commande (demande au plus tard la veille de l\'envoi_jour ouvré)',
    value: 15,
    topic: 2,
  },
  {
    name: 'Annulation de commande',
    value: 16,
    topic: 2,
  },
  {
    name: 'Utilisation, conseils et informations',
    value: 17,
    topic: 2,
  },
  {
    name: 'Réinitialisation de mot de passe',
    value: 18,
    topic: 2,
  },
  {
    name: 'Demande de formation',
    value: 19,
    topic: 2,
  },
  {
    name: 'Connexion à Zéphyr',
    value: 20,
    topic: 3,
  },
  {
    name: 'Comment passer une commande dans Zéphyr (Environnement PARTICULIERS)',
    value: 21,
    topic: 3,
  },
  {
    name: 'Comment passer une commande dans Zéphyr (Environnement ENTREPRISES)',
    value: 22,
    topic: 3,
  },
  {
    name: 'Mise à jour acteur',
    value: 23,
    topic: 3,
  },
  {
    name: 'Transfert de fichier vers Maeva',
    value: 24,
    topic: 3,
  },
  {
    name: 'Télécharger le pqsr Multi-affaires',
    value: 25,
    topic: 3,
  },
  {
    name: 'Demande de formation',
    value: 26,
    topic: 3,
  },
  {
    name: 'Télécharger le guide Marketing R1',
    value: 27,
    topic: 4,
  },
  {
    name: 'Télécharger le guide Marketing R2',
    value: 28,
    topic: 4,
  },
  {
    name: 'Télécharger le guide Signature des courriers PMR',
    value: 29,
    topic: 4,
  },
  {
    name: 'Télécharger le guide Coût et facturation',
    value: 30,
    topic: 4,
  },
  {
    name: 'Télécharger le guide performance PMR',
    value: 31,
    topic: 4,
  },
  {
    name: 'Télécharger le guide Cycle du PMR',
    value: 32,
    topic: 4,
  },
  {
    name: 'Telecharger les supports PMR courrier et email',
    value: 33,
    topic: 4,
  },
  {
    name: 'Question Signature courrier',
    value: 34,
    topic: 4,
  },
  {
    name: 'Présentation Alhéna',
    value: 35,
    topic: 4,
  },
  {
    name: 'My check up',
    value: 36,
    topic: 4,
  },
  {
    name: 'Question adresses pied de page courrier',
    value: 37,
    topic: 4,
  },
  {
    name: 'Modification adhésion PMR',
    value: 38,
    topic: 4,
  },
  {
    name: 'Comment déclarer une ouverture/fermeture d\'affaire',
    value: 39,
    topic: 4,
  },
  {
    name: 'CAMPAGNES SMS APV',
    value: 40,
    topic: 4,
  },
  {
    name: 'Contact High-co Mindoza (facturation PMR, commande de PLV APV)',
    value: 41,
    topic: 4,
  },
  {
    name: 'Autres',
    value: 42,
    topic: 4,
  },
  {
    name: 'Télécharger le guide utilisateur',
    value: 43,
    topic: 5,
  },
  {
    name: 'Vérifier IPN vendeur',
    value: 44,
    topic: 5,
  },
  {
    name: 'Créer / modifier un secteur',
    value: 45,
    topic: 5,
  },
  {
    name: 'Autres',
    value: 46,
    topic: 5,
  },
  {
    name: 'Demande de formation',
    value: 47,
    topic: 5,
  },
  {
    name: 'Télécharger le guide utilisateur',
    value: 48,
    topic: 6,
  },
  {
    name: 'Affectation de leads',
    value: 49,
    topic: 6,
  },
  {
    name: 'Contact Assistance MAEVA',
    value: 50,
    topic: 6,
  },
  {
    name: 'Autres',
    value: 51,
    topic: 6,
  },
  {
    name: 'Télécharger le guide utilisateur',
    value: 52,
    topic: 7,
  },
  {
    name: 'Télécharger le guide utilisateur',
    value: 53,
    topic: 8,
  },
  {
    name: 'Questions PLV VN',
    value: 54,
    topic: 8,
  },
  {
    name: 'Duplicata de facture',
    value: 55,
    topic: 8,
  },
  {
    name: 'Questions sur votre facture',
    value: 56,
    topic: 8,
  },
  {
    name: 'Contact One Vision (commande de brochure, accès espace personnel)',
    value: 57,
    topic: 8,
  },
  {
    name: 'Demande de formation',
    value: 58,
    topic: 8,
  },
  {
    name: 'One vision',
    value: 59,
    topic: 9,
  },
  {
    name: 'Zephyr',
    value: 60,
    topic: 9,
  },
  {
    name: 'Catalogue Renault Scope',
    value: 61,
    topic: 9,
  },
  {
    name: 'SCF',
    value: 62,
    topic: 9,
  },
];
