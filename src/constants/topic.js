export const topicList = [
  {
    name: 'Mise à jour de la base client',
    value: 1,
  },
  {
    name: 'Catalogue Renaut Scope',
    value: 2,
  },
  {
    name: 'Zephyr',
    value: 3,
  },
  {
    name: 'PMR',
    value: 4,
  },
  {
    name: 'SCF',
    value: 5,
  },
  {
    name: 'Maevra',
    value: 6,
  },
  {
    name: 'Géoreporting',
    value: 7,
  },
  {
    name: 'One Vision',
    value: 8,
  },
  {
    name: 'Demande de formation',
    value: 9,
  },
  {
    name: 'Autres',
    value: 10,
  },
];
