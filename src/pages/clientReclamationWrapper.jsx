import styled from 'styled-components';
export default (Component) => styled(Component)`
h1 {
    margin-bottom: 20px;
}
h2{
    &:last-of-type {
        margin-bottom: 0.5em;
    }
}
ul {
    margin-bottom: 3.125em;
    padding-left: 14px;
}
span {
   font-size: 0.875em;
   font-weight: 400; 
}
.section__form {
    margin-top: 3.125em;
    p {
        font-size: 0.875em;
        margin: 0 0 1em 0;
    }
    form {
        .selectdiv {
            position: relative;
            display: inline-block;
            color: #000000;
            &:after {
                content: '>';
                font: 20px "Consolas",monospace;
                -webkit-transform: rotate(90deg);
                -moz-transform: rotate(90deg);
                -ms-transform: rotate(90deg);
                -webkit-transform: rotate(90deg);
                -ms-transform: rotate(90deg);
                transform: rotate(90deg);
                right: 11px;
                top: 8px;
                position: absolute;
                pointer-events: none;
                font-weight: bold;
            }
        }
        .selectdiv-topic {
            flex-grow: 1;
            margin-right: 20px;
        }
        .selectdiv-subject {
            flex-grow: 2;
            margin-right: 20px;
        }
        .buttondiv {
            flex-basis: 15.5625em;
            position: relative;
            &:after {
                content: '>';
                font: 20px "Consolas",monospace;
                right: 11px;
                top: 8px;
                position: absolute;
                pointer-events: none;
                font-weight: bold;
            }
        }
    }
}
@media screen and (max-width: 1024px) {
    .section__form {
        form {
            .buttondiv {
                margin-top: 20px;
            }
        }
  }
}
`;
