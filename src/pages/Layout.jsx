import React from 'react';
import Header from '../components/header/header';
const Layout = ({ children }) => (
  <>
    <Header />
    <div className="container--block">
      <div className="container">{children}</div>
    </div>
  </>
);

export default Layout;
