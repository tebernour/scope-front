import React, { useState } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { RequestThemeForm } from '../components/Forms';
import DataFormSection from '../components/sections';
import ClientReclamationWrapper from './clientReclamationWrapper';

const ClientReclamation = ({ className }) => {
  const [isValidate, setIsValidate] = useState(false);
  const [topic, setTopic] = useState('');
  const [subject, setSubject] = useState('');
  const initialValues = {
    topic: '',
    subject: '',
  };
  const validationSchema = Yup.object().shape({
    topic: Yup.string().required('Le thème est obligatoire'),
    subject: Yup.string().when(['topic'], {
      is: (topic) => topic !== 'Autres' || !topic,
      then: Yup.string().required('Le sujet de la demande est obligatoire'),
    }),
  });

  return (
    <div className={className}>
      <div className="section__header">
        <h1>Nous contacter</h1>
        <h2>
          Bienvenue dans votre espace d'assistance Marketing Relationnel.
        </h2>
        <h2>Nous sommes là pour vous aider.</h2>
        <h3>
          Le pôle « Relation Réseau » de
          {' '}
          <b>Renault Scope</b>
          {' '}
          est à votre service pour :
        </h3>
        <ul>
          <li>répondre à toutes vos questions liées aux campagnes Marketing du groupe Renault,</li>
          <li>
            vous former aux différentes applications Marketing Relationnel,
          </li>
          <li>
            vous accompagner dans la mise en place de vos opérations locales.
          </li>
        </ul>
        <hr />
      </div>
      <div className="section__form">
        <p>
          Veuillez préciser l’objet de votre demande :
        </p>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={(values) => {
            setIsValidate(true);
            setTopic(values.topic);
            setSubject(values.subject);
          }}
          render={RequestThemeForm}
        />
        {isValidate ? <DataFormSection topic={topic} subject={subject} /> : <div />}

      </div>
    </div>
  );
};

export default ClientReclamationWrapper(ClientReclamation);
