import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createUploadLink } from 'apollo-upload-client';

const cache = new InMemoryCache();
const link = createUploadLink({
  uri: `${process.env.REACT_APP_BACKEND_URL}/graphql`,
});
const client = new ApolloClient({
  cache,
  link,
});

export default client;
