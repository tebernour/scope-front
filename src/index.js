import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import { BrowserRouter as Router } from 'react-router-dom';
import client from './ApolloClient';
import App from './App';
import GlobalStyle from './assets/styles/globalStyle';
import * as serviceWorker from './serviceWorker';
import { UserProvider } from './helpers/context';

ReactDOM.render(
  <ApolloProvider client={client}>
    <GlobalStyle />
    <UserProvider>
      <Router>
        <App />
      </Router>
    </UserProvider>
  </ApolloProvider>,
  document.getElementById('root')
);

serviceWorker.unregister();
