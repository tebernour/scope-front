export const formatDate = (date) => {
  let d;
  let month;
  let day;
  let year;
  if (date === undefined) {
    d = new Date();
    month = `${d.getMonth() + 1}`;
    day = `${d.getDate()}`;
    year = d.getFullYear();
    if (month.length < 2) month = `0${month}`;
    if (day.length < 2) day = `0${day}`;
    if (year.toString().length === 4 && month.length === 2 && day.length === 2) {
      return `${year}-${month}-${day}`;
    }
  }
  d = new Date(date);
  month = `${d.getMonth() + 1}`;
  day = `${d.getDate()}`;
  year = d.getFullYear();

  if (month.length < 2) month = `0${month}`;
  if (day.length < 2) day = `0${day}`;
  if (year.toString().length === 4 && month.length === 2 && day.length === 2) {
    return true;
  }
  return false;
};

export const formDate = (date) => {
  const d = new Date(date);
  let month = `${d.getMonth() + 1}`;
  let day = `${d.getDate()}`;
  const year = d.getFullYear();

  if (month.length < 2) month = `0${month}`;
  if (day.length < 2) day = `0${day}`;

  return [year.toString().substr(2, 2), month, day].join('');
};
export const formMonth = () => {
  const d = new Date();
  let month = `${d.getMonth() + 1}`;
  const year = d.getFullYear();

  if (month.length < 2) month = `0${month}`;

  return `${year}-0${d.getMonth() + 1}`;
};
