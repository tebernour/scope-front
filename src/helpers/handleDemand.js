import { handleUpload } from './handleUpload';
import { formDate, formatDate } from './formatDate';

export const handleDemand = async (
  subject,
  topic,
  demandNumber,
  createDemand,
  createUpload,
  files,
  values
) => {
  console.log('values', values);
  const { data } = await createDemand({
    variables: {
      NumeroDemande: `${formDate(Date())}–${demandNumber}`,
      dateDemande: formatDate(),
      Nom: values.nom,
      Prenom: values.prenom,
      Adresse: values.adresse,
      CodePostal: values.codepostal,
      Ville: values.ville,
      Telephone: values.telephone,
      Chassis: values.chassis,
      Email: values.email,
      Immatriculation: values.immatriculation,
      DateFermeture: values.date_fermeture === '' ? null : values.date_fermeture,
      numeroCommande: values.numero_commande,
      numeroFacture: values.numero_facture,
      Fonction: values.fonction,
      motifAnnulation: values.motif_annulation,
      RRF: values.RRF,
      dateEnvoieSouhaite: values.date_envoi === '' ? null : values.date_envoi,
      heureEnvoieSouhaite: (values.heure_envoi === '' || values.heure_envoi === undefined) ? null : `${values.heure_envoi}:00.000`,
      dateFacture: values.periode_facturation === '' ? null : new Date(values.periode_facturation),
      factureRRF: values.RRF_facture,
      texteLibre: values.text_libre,
      fichiersJoint: await handleUpload(createUpload, files),
      sujet_de_demande: subject,
      theme_de_demande: topic,
      nomUser: values.dataUser.nom,
      prenomUser: values.dataUser.prenom,
      emailUser: values.dataUser.email,
      telephoneUser: values.dataUser.telephone,
      codeRRFUser: values.dataUser.codeRRF.toString(),
      drUser: values.dataUser.codeDR.toString(),
      Response: (values.response === 'non' && values.text_libre === '') ? 'NonAvecAbandon' : values.response,
    },
  });
  return data.createDemande.demande;
};
