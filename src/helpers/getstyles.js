import { getIn } from 'formik';
export const getStyles = (errors, touched, fieldName) => {
  if (getIn(errors, fieldName) && getIn(touched, fieldName)) {
    return {
      border: '1px solid red', // on va changer notre css selon le besoin
    };
  }
  return {};
};
