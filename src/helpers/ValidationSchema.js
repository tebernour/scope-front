/* eslint-disable consistent-return */
import * as Yup from 'yup';
import {
  pickBy, isBoolean, keys, includes, isEmpty,
} from 'lodash';
import { formatDate } from './formatDate';
import { scenarioConstants } from '../constants';

export const validationSchema = (form) => {
  const makeCommentBlockMax = () => ({
    is: 'non',
    then: Yup.string()
      .max(900, 'Le champ commentaire doit comporter au plus 900 caractères.'),
  });

  const formFields = keys(pickBy(form, isBoolean));
  const userFields = ['nom', 'prenom', 'email', 'telephone', 'codeRRF', 'codeDR'];
  if (isEmpty(form)) {
    return Yup.object().shape({
      text_libre: Yup.string()
        .required('Ce champ est obligatoire')
        .max(900, 'Le champ commentaire doit comporter au plus 900 caractères.'),
      dataUser: Yup.object().shape({
        prenom: Yup.string().when([], {
          is: () => includes(userFields, 'prenom'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .max(50, 'Le prénom doit comporter au plus 50 caractères.')
            .test('prenom', 'Le nom doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \']', (value) => {
              if (value) {
                const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
                return (regex.test(value));
              }
              return false;
            }),
        }),
        nom: Yup.string().when([], {
          is: () => includes(userFields, 'nom'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .max(50, 'Le nom doit comporter au plus 50 caractères.')
            .test('nom', 'Le nom doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \']', (value) => {
              if (value) {
                const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
                return (regex.test(value));
              }
              return false;
            }),
        }),
        email: Yup.string().when([], {
          is: () => includes(userFields, 'prenom'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .email('L\'email doit comporter un @ ainsi qu\'une extension')
            .email('Veuillez entrer une adresse email valide.')
            .test('email', 'L\'email doit comporter des caractères alphanumeriques sans accent ', (value) => {
              if (value) {
                const regex = /^[a-zA-Z0-9-+_.@']*$/;
                return (regex.test(value));
              }
              return value;
            }),
        }),
        telephone: Yup.string().when([], {
          is: () => includes(userFields, 'telephone'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .test('telephone', 'Le numéro de téléphone doit comporter 10 chiffres',
              (value) => {
                if (value) {
                  const regex = /^-?[\d.]+(?:e-?\d+)?$/;
                  return (value.toString().length <= 10
                    && value.toString().length >= 9 && regex.test(value));
                }
                return false;
              }),
        }),
        codeDR: Yup.string().when([], {
          is: () => includes(userFields, 'codeDR'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .test('codeDR', 'Code DR  doit comprendre au maximum 8 chiffres', (value) => {
              if (value) {
                const isNumber = /^\d+$/.test(value);
                return (value.length <= 8 && isNumber);
              }
              return false;
            }),
        }),
        codeRRF: Yup.string().when([], {
          is: () => includes(userFields, 'codeRRF'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .test('codeRRF', 'Le code RRF doit comporter 8 chiffre', (value) => {
              if (value) {
                const isNumber = /^\d+$/.test(value);
                return (value.length <= 8 && isNumber);
              }
              return false;
            }),
        }),
      }),
    });
  }
  if (form.scenario && form.scenario.nom === scenarioConstants.scenario3) {
    return Yup.object().shape({
      response: Yup.string()
        .required('Merci de bien vouloir donner votre réponse.'),
      text_libre: Yup.string()
        .when('response', makeCommentBlockMax()),
      dataUser: Yup.object().shape({
        prenom: Yup.string().when([], {
          is: () => includes(userFields, 'prenom'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .max(50, 'Le prénom doit comporter au plus 50 caractères.')
            .test('prenom', 'Le nom doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \' "', (value) => {
              if (value) {
                const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
                return (regex.test(value));
              }
              return false;
            }),
        }),
        nom: Yup.string().when([], {
          is: () => includes(userFields, 'prenom'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .max(50, 'Le nom doit comporter au plus 50 caractères.')
            .test('prenom', 'Le nom doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \' "', (value) => {
              if (value) {
                const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
                return (regex.test(value));
              }
              return false;
            }),
        }),
        email: Yup.string().when([], {
          is: () => includes(userFields, 'prenom'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .email('L\'email doit comporter un @ ainsi qu\'une extension')
            .email('Veuillez entrer une adresse email valide.')
            .test('email', 'L\'email doit comporter des caractères alphanumeriques sans accent ', (value) => {
              if (value) {
                const regex = /^[a-zA-Z0-9-+_.@']*$/;
                return (regex.test(value));
              }
              return value;
            }),
        }),
        telephone: Yup.string().when([], {
          is: () => includes(userFields, 'telephone'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .test('nombre caractères', 'Le numéro de téléphone doit comporter 10 chiffres',
              (value) => {
                if (value) {
                  const regex = /^-?[\d.]+(?:e-?\d+)?$/;
                  return (value.toString().length <= 10
                    && value.toString().length >= 9 && regex.test(value));
                }
                return false;
              }),
        }),
        codeDR: Yup.string().when([], {
          is: () => includes(userFields, 'codeDR'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .test('codeDR', 'Code DR  doit comprendre au maximum 8 chiffres', (value) => {
              if (value) {
                const isNumber = /^\d+$/.test(value);
                return (value.length <= 8 && isNumber);
              }
              return false;
            }),
        }),
        codeRRF: Yup.string().when([], {
          is: () => includes(userFields, 'codeRRF'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .test('codeRRF', 'Le code RRF doit comporter 8 chiffres', (value) => {
              if (value) {
                const isNumber = /^\d+$/.test(value);
                return (value.length <= 8 && isNumber);
              }
              return false;
            }),
        }),
      }),
    });
  }
  if (keys(form).length === 1 && form.text_libre) {
    return Yup.object().shape({
      text_libre: Yup.string()
        .required('Ce champ est obligatoire')
        .max(900, 'Le champ commentaire doit comporter au plus 900 caractères.'),
      dataUser: Yup.object().shape({
        prenom: Yup.string().when([], {
          is: () => includes(userFields, 'prenom'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .max(50, 'Le prénom doit comporter au plus 50 caractères.')
            .test('prenom', 'Le nom doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \' "', (value) => {
              if (value) {
                const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
                return (regex.test(value));
              }
              return false;
            }),
        }),
        nom: Yup.string().when([], {
          is: () => includes(userFields, 'prenom'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .max(50, 'Le nom doit comporter au plus 50 caractères.')
            .test('prenom', 'Le nom doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \' "', (value) => {
              if (value) {
                const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
                return (regex.test(value));
              }
              return false;
            }),
        }),
        email: Yup.string().when([], {
          is: () => includes(userFields, 'prenom'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .email('L\'email doit comporter un @ ainsi qu\'une extension')
            .email('Veuillez entrer une adresse email valide.')
            .test('email', 'L\'email doit comporter des caractères alphanumeriques sans accent ', (value) => {
              if (value) {
                const regex = /^[a-zA-Z0-9-+_.@']*$/;
                return (regex.test(value));
              }
              return value;
            }),
        }),
        telephone: Yup.string().when([], {
          is: () => includes(userFields, 'telephone'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .test('nombre caractères', 'Le numéro de téléphone doit comporter 10 chiffres',
              (value) => {
                if (value) {
                  const regex = /^-?[\d.]+(?:e-?\d+)?$/;
                  return (value.toString().length <= 10
                    && value.toString().length >= 9 && regex.test(value));
                }
                return false;
              }),
        }),
        codeDR: Yup.string().when([], {
          is: () => includes(userFields, 'codeDR'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .test('codeDR', 'Code DR  doit comprendre au maximum 8 chiffres', (value) => {
              if (value) {
                const isNumber = /^\d+$/.test(value);
                return (value.length <= 8 && isNumber);
              }
              return false;
            }),
        }),
        codeRRF: Yup.string().when([], {
          is: () => includes(userFields, 'codeRRF'),
          then: Yup.string()
            .required('Ce champ est obligatoire')
            .test('codeRRF', 'Le code RRF doit comporter 8 chiffres', (value) => {
              if (value) {
                const isNumber = /^\d+$/.test(value);
                return (value.length <= 8 && isNumber);
              }
              return false;
            }),
        }),
      }),
    });
  }
  return Yup.object().shape({
    nom: Yup.string().when([], {
      is: () => includes(formFields, 'nom'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .max(50, 'Le nom doit comporter au plus 50 caractères.')
        .test('nom', 'Le nom doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \' ', (value) => {
          if (value) {
            const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
            return (regex.test(value));
          }
          return false;
        }),
    }),
    prenom: Yup.string().when([], {
      is: () => includes(formFields, 'prenom'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .max(50, 'Le prenom doit comporter au plus 50 caractères')
        .test('prenom', 'Le prénom doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \']', (value) => {
          if (value) {
            const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
            return (regex.test(value));
          }
          return value;
        }),
    }),
    email: Yup.string().when([], {
      is: () => includes(formFields, 'email'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .email('L\'email doit comporter un @ ainsi qu\'une extension')
        .email('Veuillez entrer une adresse email valide.')
        .test('email', 'L\'email doit comporter des caractères alphanumeriques sans accent ', (value) => {
          if (value) {
            const regex = /^[a-zA-Z0-9-+_.@']*$/;
            return (regex.test(value));
          }
          return value;
        }),
    }),
    fonction: Yup.string().when([], {
      is: () => includes(formFields, 'fonction'),
      then: Yup.string()
        .required('Ce champ est obligatoire'),
    }),
    RRF: Yup.string().when([], {
      is: () => includes(formFields, 'RRF'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .test('RRF', 'Le code RRF doit comporter 8 chiffres', (value) => {
          if (value) {
            const isNumber = /^\d+$/.test(value);
            return (value.length === 8 && isNumber);
          }
          return false;
        }),
    }),
    RRF_facture: Yup.string().when([], {
      is: () => includes(formFields, 'RRF_facture'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .test('RRF_facture', 'Le code RRF du facture doit comporter 8 chiffres', (value) => {
          if (value) {
            const isNumber = /^\d+$/.test(value);
            return (value.length === 8 && isNumber);
          }
          return false;
        }),
    }),
    adresse: Yup.string().when([], {
      is: () => includes(formFields, 'adresse'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .test('adresse', 'L\'adresse doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \']',
          (value) => {
            if (value) {
              const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
              return (regex.test(value));
            }
            return false;
          }),
    }),
    codepostal: Yup.string().when([], {
      is: () => includes(formFields, 'code_postal'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .test('codepostal', 'Le code postal doit comporter 4 ou 5 chiffres', (value) => {
          if (value) {
            const regex = /^[0-9]+$/;
            return (
              value.toString().length <= 5 && value.toString().length >= 4 && regex.test(value)
            );
          }
          return false;
        }),
    }),
    ville: Yup.string().when([], {
      is: () => includes(formFields, 'ville'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .min(1, 'doit comporter au moins 1 caractère.')
        .max(200, 'La ville doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \'] ')
        .test('ville', 'La ville doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \'] ',
          (value) => {
            if (value) {
              const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
              return (regex.test(value));
            }
            return false;
          }),
    }),
    telephone: Yup.string().when([], {
      is: () => includes(formFields, 'telephone'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .test('telephone', 'Le numéro de téléphone doit comporter 10 chiffres',
          (value) => {
            if (value) {
              const regex = /^-?[\d.]+(?:e-?\d+)?$/;
              return (value.toString().length === 10 && regex.test(value));
            }
            return false;
          }),
    }),
    SMS: Yup.string().when([], {
      is: () => includes(formFields, 'SMS'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .test('SMS', 'Le numéro de téléphone (SMS) doit comporter 10 chiffres',
          (value) => {
            if (value) {
              const regex = /^-?[\d.]+(?:e-?\d+)?$/;
              return (value.toString().length === 10 && regex.test(value));
            }
            return false;
          }),
    }),
    chassis: Yup.string().when([], {
      is: () => includes(formFields, 'chassis'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .test('chassis', 'Le châssis doit comporter 17 caractères alphanumériques',
          (value) => {
            if (value) {
              return (value.length === 17);
            }
            return false;
          }),
    }),
    immatriculation: Yup.string().when([], {
      is: () => includes(formFields, 'immatriculation'),
      then: Yup.string()
        .test('immatriculation', 'Le numéro d\'immatriculation doit comporter 7 à 9 caractères alphanumériques.',
          (value) => {
            console.log('validationSchema -> value', value);
            if (value) {
              const regex = /^[a-zA-Z0-9]+$/i;
              return (value.toString().length >= 7 && regex.test(value));
            }
            return true;
          }),
    }),
    numero_commande: Yup.string().when([], {
      is: () => includes(formFields, 'numero_commande'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .test('numero_commande', 'Le numéro de commande doit comporter 15 caractères alphanumériques',
          (value) => {
            if (value) {
              const regex = /^[a-zA-Z0-9]+$/i;
              return (value.length === 15 && regex.test(value));
            }
          }),
    }),
    numero_facture: Yup.string().when([], {
      is: () => includes(formFields, 'numero_facture'),
      then: Yup.string()
        .test('numero_facture', 'Le numéro de facture doit comporter 8 caractères alphanumériques',
          (value) => {
            if (value) {
              const regex = /^[a-zA-Z0-9]+$/i;
              return (value.length === 8 && regex.test(value));
            }
            return true;
          }),
    }),
    motif_annulation: Yup.string().when([], {
      is: () => includes(formFields, 'motif_annulation'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .max(250, 'Motif d\'annulation doit comporter au plus 250 caractères.'),
    }),
    text_libre: Yup.string().when([], {
      is: () => includes(formFields, 'text_libre'),
      then: Yup.string()
        .max(900, 'Le champ commentaire doit comporter au plus 900 caractères.'),
    }),
    date_envoi: Yup.string().when([], {
      is: () => includes(formFields, 'date_envoi'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .nullable(true),
    }),
    heure_envoi: Yup.string()
      .when([], {
        is: () => includes(formFields, 'date_envoi'),
        then: Yup.string()
          .required('Ce champ est obligatoire')
          .nullable(),
      }),
    periode_facturation: Yup.string().when([], {
      is: () => includes(formFields, 'periode_facturation'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .nullable(),
    }),
    date_fermeture: Yup.string().when([], {
      is: () => includes(formFields, 'date_fermeture'),
      then: Yup.string()
        .required('Ce champ est obligatoire')
        .test('date fermeture', 'Format de la date de fermeture  invalide', (value) => {
          if (value) {
            return formatDate(value);
          }
          return false;
        })
        .nullable(),
    }),
    dataUser: Yup.object().shape({
      prenom: Yup.string().when([], {
        is: () => includes(userFields, 'prenom'),
        then: Yup.string()
          .required('Ce champ est obligatoire')
          .max(50, 'Le prénom doit comporter au plus 50 caractères.')
          .test('prenom', 'Le nom doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \'] ', (value) => {
            if (value) {
              const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
              return (regex.test(value));
            }
            return false;
          }),
      }),
      nom: Yup.string().when([], {
        is: () => includes(userFields, 'nom'),
        then: Yup.string()
          .required('Ce champ est obligatoire')
          .max(50, 'Le nom doit comporter au plus 50 caractères.')
          .test('nom', 'Le nom doit comporter des caractères alphabétiques plus les caractères :" à â ç è é ê î ô ù û - \'] ', (value) => {
            if (value) {
              const regex = /[a-zA-Z-à-â-ç-è-é-ê-î-ô-ù-û-+']+\s*$/;
              return (regex.test(value));
            }
            return false;
          }),
      }),
      email: Yup.string().when([], {
        is: () => includes(userFields, 'email'),
        then: Yup.string()
          .required('Ce champ est obligatoire')
          .email('L\'email doit comporter un @ ainsi qu\'une extension')
          .email('Veuillez entrer une adresse email valide.')
          .test('email', 'L\'email doit comporter des caractères alphanumeriques sans accent ', (value) => {
            if (value) {
              const regex = /^[a-zA-Z0-9-_+.@']*$/;
              return (regex.test(value));
            }
            return value;
          }),
      }),

      telephone: Yup.string().when([], {
        is: () => includes(userFields, 'telephone'),
        then: Yup.string()
          .required('Ce champ est obligatoire')
          .test('nombre caractères', 'Le numéro de téléphone doit comporter 10 chiffres',
            (value) => {
              if (value) {
                const isNumber = /^\d+$/.test(value);
                return (value.length === 10 && isNumber);
              }
              return true;
            }),
      }),
      codeDR: Yup.string().when([], {
        is: () => includes(userFields, 'codeDR'),
        then: Yup.string()
          .required('Ce champ est obligatoire')
          .test('codeDR', 'Code DR  doit comprendre au maximum 8 chiffres', (value) => {
            if (value) {
              const isNumber = /^\d+$/.test(value);
              return (value.length <= 8 && isNumber);
            }
            return false;
          }),
      }),
      codeRRF: Yup.string().when([], {
        is: () => includes(userFields, 'codeRRF'),
        then: Yup.string()
          .required('Ce champ est obligatoire')
          .test('codeRRF', 'Le code RRF doit comporter 8 chiffres', (value) => {
            if (value) {
              const isNumber = /^\d+$/.test(value);
              return (value.length <= 8 && isNumber);
            }
            return false;
          }),
      }),
    }),
  });
};
