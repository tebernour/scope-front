/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
import {
  find, reduce, includes, filter, keys,
  omitBy, isNull,
} from 'lodash';

export const fetchScenario = (
  subjects, topic, subject
) => {
  const filtredSubject = find(subjects, { topic: { nom: topic }, nom: subject });
  const scenario = filtredSubject && filtredSubject.formulaires
    && filtredSubject.formulaires.scenario
    && filtredSubject.formulaires.scenario.nom;
  return scenario;
};

export const fetchFields = (
  subjects, topic, subject
) => {
  const filtredSubject = find(subjects, { topic: { nom: topic }, nom: subject });
  const filtredKeys = filter(keys(filtredSubject && filtredSubject.formulaires),
    (key) => filtredSubject.formulaires[key]);
  const formFields = reduce(filtredKeys, (map, item) => {
    if (!includes(
      ['scenario', 'nom_formulaire', 'fichier_PDF', 'theme_demande', 'sujet_demande', 'scenario', '__typename'],
      item
    )
    ) {
      return {
        ...map,
        [item]: true,
      };
    }
    return { ...map };
  }, {});
  return formFields;
};
export const fetchPDF = (subjects, topic, subject) => {
  const filtredSubject = find(subjects, { topic: { nom: topic }, nom: subject });
  const form = omitBy(filtredSubject && filtredSubject.formulaires, isNull);
  return form;
};
