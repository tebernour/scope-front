/* eslint-disable no-param-reassign */
import { omitBy, isEmpty } from 'lodash';
export const sendEmail = (data) => {
  const valuesSend = omitBy(data, isEmpty);
  const requestOptions = {
    method: 'POST',
    body: JSON.stringify(valuesSend),
    headers: {
      'Content-Type': 'application/json',
    },
  };
  fetch(`${process.env.REACT_APP_API_URL}/sendemail`, requestOptions);
};
