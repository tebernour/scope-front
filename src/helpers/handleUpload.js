export const handleUpload = async (createUpload, files) => {
  const { data } = await createUpload({
    variables: {
      files: files.map((file) => file),
    },

  });
  return data.multipleUpload.map((upload) => upload.id);
};
