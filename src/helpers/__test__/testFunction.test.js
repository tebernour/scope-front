/* eslint-disable no-undef */
import testFunction from '../testFunction';

describe('test testFunction', () => {
  it('should return a+b', () => {
    const a = 5;
    const b = 6;
    const result = 11;
    expect(testFunction(a, b)).toEqual(result);
  });

  it('should return a-b', () => {
    const a = 6;
    const b = 5;
    const result = 1;
    expect(testFunction(a, b)).toEqual(result);
  });
});
