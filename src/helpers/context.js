import React, { useState, useEffect, createContext } from 'react';

export const UserContext = createContext();

export const UserProvider = (props) => {
  const requestOptions = {
    method: 'GET',
  };
  const [apiResponse, setApiResponse] = useState({});
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/fetchdata`, requestOptions)
      .then((response) => response.json())
      .then((data) => setApiResponse(data))
      .then((error) => error);
  }, []);

  return (
    <UserContext.Provider value={apiResponse}>
      {props.children}
    </UserContext.Provider>
  );
};
