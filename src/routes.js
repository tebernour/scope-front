import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ClientReclamation from './pages/clientReclamation';

const RoutesList = [
  {
    name: 'contact',
    pathIsExact: true,
    path: '/',
    component: ClientReclamation,
  },
];

const Routes = () => (
  <Switch>
    {RoutesList.map((route) => (
      <Route
        key={route.name}
        exact={route.pathIsExact}
        path={route.path}
        component={route.component}
      />
    ))}
  </Switch>
);

export default Routes;
