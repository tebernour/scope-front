import gql from 'graphql-tag';

export const UPLOAD_MUTATION = gql`
  mutation multipleUpload($files: [Upload]!) {
    multipleUpload(files: $files) {
      id
    }
  }
`;
