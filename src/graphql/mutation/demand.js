import gql from 'graphql-tag';

export const DEMAND_MUTATION = gql`
  mutation createDemande(
    $NumeroDemande: String
    $dateDemande: Date
    $Nom: String
    $Prenom: String
    $Adresse: String
    $CodePostal: String
    $Ville: String
    $Telephone: String
    $Chassis: String
    $Email: String
    $Immatriculation: String
    $DateFermeture: Date
    $numeroCommande: String
    $numeroFacture: String
    $Fonction: String
    $motifAnnulation: String
    $RRF: String
    $dateEnvoieSouhaite: Date
    $heureEnvoieSouhaite: Time
    $dateFacture: DateTime
    $texteLibre: String
    $fichiersJoint: [ID]
    $sujet_de_demande: String
    $theme_de_demande: String
    $nomUser: String
    $prenomUser: String
    $emailUser: String
    $codeRRFUser: String
    $drUser: String
    $telephoneUser: String
    $Response: ENUM_DEMANDES_RESPONSE
    $factureRRF: String
  ) {
    createDemande(input:{ data :{
      NumeroDemande: $NumeroDemande
      dateDemande: $dateDemande
      Nom: $Nom
      Prenom: $Prenom
      Adresse: $Adresse
      CodePostal: $CodePostal
      Ville: $Ville
      Telephone: $Telephone
      Chassis: $Chassis
      Email: $Email
      Immatriculation: $Immatriculation
      DateFermeture: $DateFermeture
      numeroCommande: $numeroCommande
      numeroFacture: $numeroFacture
      Fonction: $Fonction
      motifAnnulation: $motifAnnulation
      RRF: $RRF
      dateEnvoieSouhaite: $dateEnvoieSouhaite
      heureEnvoieSouhaite: $heureEnvoieSouhaite
      texteLibre: $texteLibre
      fichiersJoint: $fichiersJoint
      sujet_de_demande: $sujet_de_demande
      theme_de_demande: $theme_de_demande
      nomUser:$nomUser
      prenomUser:$prenomUser
      emailUser:$emailUser
      codeRRFUser:$codeRRFUser
      drUser:$drUser
      telephoneUser:$telephoneUser
      Response:$Response
      dateFacture: $dateFacture
      factureRRF: $factureRRF
    }}
    ) {
      demande {
        NumeroDemande
        dateDemande
        Nom
        Prenom
        Adresse
        CodePostal
        Ville
        Telephone
        Chassis
        Email
        Immatriculation
        DateFermeture
        numeroCommande
        numeroFacture
        Fonction
        motifAnnulation
        RRF
        dateEnvoieSouhaite
        heureEnvoieSouhaite
        dateFacture
        factureRRF
        texteLibre
        fichiersJoint {
          name
          url
        }
        sujet_de_demande
        theme_de_demande
        nomUser
        prenomUser
        emailUser
        codeRRFUser
        drUser
        telephoneUser
      }
    }
  }
`;
