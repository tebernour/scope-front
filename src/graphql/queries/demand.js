import gql from 'graphql-tag';

export const DEMAND_QUERY = gql`
  query demandes {
    demandes(limit: 1 sort: "createdAt:DESC") {
      NumeroDemande
    }
  }
`;
