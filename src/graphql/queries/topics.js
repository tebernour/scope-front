import gql from 'graphql-tag';

export const TOPICS_QUERY = gql`
query topics {
  topics {
    id
    nom
    subjects {
      nom
    }
  }
}

`;
