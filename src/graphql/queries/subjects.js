import gql from 'graphql-tag';

export const SUBJECT_QUERY = gql`
query subjects {
  subjects {
    id
    nom
    topic {
      id
      nom
    }
    formulaires {
      nom_formulaire
      nom
      prenom
      adresse
      ville
      chassis
      date_fermeture
      email
      SMS
      telephone
      fonction
      RRF
      numero_commande
      date_envoi
      motif_annulation
      numero_facture
      RRF_facture
      periode_facturation
      code_postal
      text_libre
      texte_reponse
      immatriculation
      libelle_process
      fichier_PDF {
        name
        url
        id
      }
      scenario {
        nom
      }
    }
  }
}

`;
