/* eslint-disable no-unused-expressions */
import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import DataFormWrapper from './dataFormWrapper';
import { SUBJECT_QUERY } from '../../graphql/queries';
import { DataForm, FreeTextForm, ProcessForm } from '../Forms';
import { scenarioConstants } from '../../constants';
import { fetchScenario, fetchFields, fetchPDF } from '../../helpers';

const DataFormSection = ({ className, topic, subject }) => {
  const { data: subjectData, loading } = useQuery(SUBJECT_QUERY);
  return (
    <div className={className}>
      {(loading) ? <div>loading ... </div> : (
        <div className="section-data">
          {
            fetchScenario(subjectData.subjects, topic, subject) === scenarioConstants.scenario1
            && (
              <DataForm
                form={fetchFields(subjectData.subjects, topic, subject)}
                subject={subject}
                topic={topic}
              />
            )
          }
          {
            (fetchScenario(subjectData.subjects, topic, subject) === scenarioConstants.scenario2 || topic === 'Autres')
              && (
                <FreeTextForm
                  form={fetchFields(subjectData.subjects, topic, subject)}
                  subject={subject}
                  topic={topic}
                />
              )
          }
          {
            fetchScenario(subjectData.subjects, topic, subject) === scenarioConstants.scenario3
            && (
              <ProcessForm
                form={fetchPDF(subjectData.subjects, topic, subject)}
                subject={subject}
                topic={topic}
              />
            )
          }
        </div>
      )}
    </div>
  );
};
export default DataFormWrapper(DataFormSection);
