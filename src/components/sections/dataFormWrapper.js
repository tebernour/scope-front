import styled from 'styled-components';
import colors from '../../assets/styles/colors';
import urlBackground from '../../assets/images/background.svg';
export default (Component) => styled(Component)`
    .section-data {
        margin-top: 35px;
        .section-data__label {
            color: #000000;
            font-size: 14px;
            text-align: left;
            font-weight: 400;
            margin-top: 17px;
        }
        form, .content {
            margin-top: 16px;
            .button-content {
                display: flex;
                flex-basis: 18.6em;
                &:after {
                    content: '>';
                    font: 20px "Consolas",monospace;
                    left: -24px;
                    top: 8px;
                    position: relative;
                    pointer-events: none;
                    font-weight: bold;
                }
            }
            
        }
        .process-content {
            .content-header {
                font-size: 24px;
                line-height: 28px;
            }
            .content {
                display: flex;
                justify-content: space-between;

                .response-label {
                    margin-bottom: 20px;
                    height: 300px;
                }
                .process-content-file {
                    background: ${colors.lightGray};
                    padding: 30px;
                    .react-pdf__Page__canvas {
                        width: auto !important;
                        height: auto !important;
                        max-width: 250px;
                        max-height: 250px;
                    }
                    .react-pdf__Page__textContent {
                        width: unset;
                        height: unset;
                    }
                }
                .process-content-response {
                    flex-basis:669px;
                    margin-left: 20px;
                    position: relative;
                    .button-content {
                        position: absolute;
                        bottom: 0;
                        width: 297px;
                    }
                }
            }
            .process-request {
                background: url(${urlBackground})  0% 0% no-repeat padding-box;;
                width: 890px;
                height: 73px;
                color: ${colors.veryWhite};
                padding: 14px 20px;
                font-size: 14px;
                line-height: 17px;
                display: inline-block;
                margin-top: 40px;
                margin-bottom: 1em;
                    p{
                     font-size: 14px;
                     display: block;
                     margin-bottom: 0.1em;
                     color: ${colors.white}
                 }
                .form-group {
                    display: inline-block;
                    width: 90%; 
                    margin-bottom: 6px;
                    &:first-child {
                        margin-top: 10px;
                    }
                }
                .form-group  input{
                    border:0px;
                }
                .form-group .section-data__label {
                    position: absolute;
                    margin-left: 20px;
                    color: #fff;
                    border: 0px;
                    margin-top: -1px;
                }
            }
        }
    }
    @media screen and (max-width: 1024px) {
    .section-data {
        form {
            .form-group:nth-child(2n) {
                margin-right: 0;
            }
        }
    }
}


`;
