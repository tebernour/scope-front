/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, {
  useContext, useState, useEffect, useRef,
} from 'react';
import axios from 'axios';
import { Formik, Form } from 'formik';
import { isEmpty, isNull } from 'lodash';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { Twemoji } from 'react-emoji-render';
import {
  Input, FileInput, UserInformation, TextArea, useModal, PDFViewer,
} from '../commons';
import {
  validationSchema, UserContext, getStyles, handleDemand, sendEmail,
} from '../../helpers';
import xls from '../../assets/images/xls.svg';
import doc from '../../assets/images/doc.svg';
import pdf from '../../assets/images/pdf.svg';
import { DEMAND_MUTATION, UPLOAD_MUTATION } from '../../graphql/mutation';
import { DEMAND_QUERY } from '../../graphql/queries';

export const ProcessForm = ({ form, subject, topic }) => {
  const fileUrl = form.fichier_PDF && form.fichier_PDF.length > 0 && `${form.fichier_PDF[0].url}`;
  const fileName = form.fichier_PDF && form.fichier_PDF.length > 0 && form.fichier_PDF[0].name;
  const fileExtension = fileName && fileName.split('.').pop();
  const downloadPDF = (fileUrl, fileName) => {
    axios({
      url: fileUrl,
      method: 'GET',
      responseType: 'blob',
    })
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
      });
  };
  const { data } = useContext(UserContext);
  const [files, setFiles] = useState([]);
  const { setModal } = useModal();
  const initialValues = {
    response: '',
    text_libre: '',
    dataUser: {
      nom: data.firstname,
      prenom: data.lastname,
      telephone: data.phone,
      // email: data && data.email,
      email: '',
      codeRRF: data.rrf,
      codeDR: data.code_dr,
    },
  };
  const [createUpload] = useMutation(UPLOAD_MUTATION);
  const { data: demandList, loading, refetch } = useQuery(DEMAND_QUERY);
  const demandNumber = !loading && demandList.demandes
    && demandList.demandes.length > 0
    && demandList.demandes[0].NumeroDemande
    ? Number(demandList.demandes[0].NumeroDemande.split('–')[1]) + 1
    : 1;
  const [createDemand] = useMutation(DEMAND_MUTATION, {
    refetchQueries: [{ query: DEMAND_QUERY }],
  });
  const sendRequest = async (values) => {
    const data = await handleDemand(
      subject,
      topic,
      demandNumber,
      createDemand,
      createUpload,
      files,
      values
    );

    if (values.response === 'oui') {
      return setModal(<span><Twemoji text="Merci pour votre réponse. L'équipe Renault Scope vous souhaite une agréable journée :)" /></span>);
    }
    sendEmail(data);
    return setModal(
      <span>
        <p>{`Votre demande n° ${data.NumeroDemande} a bien été envoyée.`}</p>
        <p>{'Nous vous répondrons sous 48 h.'}</p>
        <p>{'Merci d\'avoir contacté Renault Scope.'}</p>
      </span>
    );
  };
  const validationSchemaa = validationSchema(form);
  const formRef = useRef({});
  useEffect(() => {
    if (!isEmpty(formRef) && !isNull(formRef.current)) {
      formRef.current.resetForm();
      setFiles([]);
    }
    refetch();
  }, [form]);
  return (
    <>
      <div className="process-content">
        <span className="content-header"> Notre réponse</span>
        <div className="content">
          {form.fichier_PDF && form.fichier_PDF.length > 0 && (
            <div className="process-content-file">
              { fileExtension === 'pdf'
                ? <PDFViewer pdf={fileUrl} />
                : (fileExtension.startsWith('xls')
                  ? (
                    <img
                      alt={fileName}
                      src={xls}
                      onClick={() => { window.open(fileUrl); }}
                    />
                  )
                  : (fileExtension.startsWith('doc')
                    ? (
                      <img
                        alt={fileName}
                        src={doc}
                      />
                    )
                    : (
                      <img
                        alt={fileName}
                        src={pdf}
                        onClick={() => { window.open(fileUrl); }}
                      />
                    )
                  )
                )}
            </div>
          )}
          <div className="process-content-response">
            <div className="response-label">
              <div
                // eslint-disable-next-line react/no-danger
                dangerouslySetInnerHTML={{
                  __html: `<pre> <span>${form.libelle_process}</span></pre>`.replaceAll(':"', ':"<br/>')
                }}
              />
            </div>
            {
              fileUrl && (
                <div className="button-content">
                  <button
                    type="button"
                    className="btn-send"
                    onClick={() => { downloadPDF(fileUrl, fileName); }}
                  >
                    télécharger
                  </button>
                </div>
              )
            }

          </div>
        </div>

      </div>
      <Formik
        innerRef={formRef}
        initialValues={initialValues}
        validationSchema={validationSchemaa}
        onSubmit={(values) => sendRequest(values)}
        render={({
          values, errors, touched, setFieldValue,
        }) => (
          <Form>
            <div className="process-content">
              <div className="process-request" style={getStyles(errors, touched, 'response')}>
                <p>Cette réponse correspond-elle à votre besoin ?</p>
                <Input
                  type="radio"
                  id="oui"
                  name="response"
                  value="oui"
                  label="Oui"
                  hideError
                />
                <Input
                  type="radio"
                  id="non"
                  name="response"
                  value="non"
                  label=" Non"
                />
              </div>
            </div>
            {values.response === 'non'
              ? (
                <>
                  <div className="row">
                    <TextArea
                      name="text_libre"
                      rows="8"
                      value={values.text_libre}
                      label="Pour mieux vous aider merci d'indiquer votre commentaire ou votre demande"
                      errors={errors}
                      touched={touched}
                      style={getStyles(errors, touched, 'text_libre')}
                    />
                  </div>
                  <div className="file-wrapper">
                    <FileInput files={files} setFiles={setFiles} />
                  </div>
                </>
              )
              : null}
            <>
              <div className="row">
                <span> Complétez ou modifiez vos coordonnées de contact :</span>
              </div>
              <UserInformation
                values={values.dataUser}
                setFieldValue={setFieldValue}
                errors={errors}
                touched={touched}
                style={getStyles(errors, touched, 'dataUser')}
              />
              <div className="button-content">
                <button
                  type="submit"
                  className="btn-send"
                >
                  Envoyer
                </button>
              </div>
            </>
          </Form>
        )}
      />
    </>
  );
};
