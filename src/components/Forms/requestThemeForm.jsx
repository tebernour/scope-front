import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { Field, Form, ErrorMessage } from 'formik';
import { isEmpty, find } from 'lodash';
// import { topicList, subjectList } from '../../constants';
// on la laisse pour le moment parce que nous avons pas tous le link avec BO
import { getStyles } from '../../helpers';
import { TOPICS_QUERY } from '../../graphql/queries';

export const RequestThemeForm = ({
  handleSubmit, values, errors, touched,
}) => {
  const { data: topicsData, loading } = useQuery(TOPICS_QUERY);
  let topics = [];
  if (topicsData) {
    topics = topics.concat(topicsData.topics);
  }
  topics.sort((a, b) => (a.nom.toUpperCase() > b.nom.toUpperCase()) ? 1 : -1);
  const filtredSubjects = find(topicsData && topicsData.topics, { nom: values.topic });
  let subjects = [];
  if (filtredSubjects) {
    subjects = subjects.concat(filtredSubjects.subjects);
  }
  subjects.sort((a, b) => (a.nom.toUpperCase() > b.nom.toUpperCase()) ? 1 : -1);
  return (
    <>
      {
        (loading) ? (<div>loading ... </div>) : (
          <Form onSubmit={handleSubmit}>
            <div className="selectdiv selectdiv-topic">
              <Field
                name="topic"
                style={getStyles(errors, touched, 'topic')}
                as="select"
                value={values.topic}
              >
                <option
                  key={0}
                  value=""
                  hidden
                  selected
                >
                  Thème de votre demande
                </option>
                {topics && topics.map((item, index) => (
                  <option
                    key={index}
                    value={item.nom}
                  >
                    {item.nom}
                  </option>
                ))}
              </Field>
              <span className="error"><ErrorMessage name="topic" /></span>
            </div>
            <div className="selectdiv selectdiv-subject">
              <Field
                name="subject"
                as="select"
                style={getStyles(errors, touched, 'subject')}
                value={values.subject}
                disabled={values.topic === '' || values.topic === 'Autres'}
              >
                <option
                  key={0}
                  value=""
                  hidden
                  selected
                >
                  {values.topic === 'Autres' ? 'Autre' : 'Sujet de votre demande'}
                </option>
                {
                  filtredSubjects && subjects.map((item, index) => (
                    <option
                      key={index}
                      value={item.nom}
                    >
                      {item.nom}
                    </option>
                  ))
                }

              </Field>
              <span className="error"><ErrorMessage className="error" name="subject" /></span>
            </div>
            <div className="buttondiv">
              <button type="submit" disabled={(values.topic === '' && values.subject === '') || !isEmpty(errors)}>
                Valider
              </button>
            </div>
          </Form>
        )
      }

    </>
  );
};
