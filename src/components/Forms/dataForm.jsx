/* eslint-disable camelcase */
import React, {
  useContext, useState, useEffect, useRef,
} from 'react';
import { Formik, Form } from 'formik';
import { isEmpty, isNull } from 'lodash';
import { useMutation, useQuery } from '@apollo/react-hooks';
import {
  Input, FileInput, useModal, UserInformation, TextArea,
} from '../commons';
import {
  getStyles,
  validationSchema,
  UserContext,
  sendEmail,
  handleDemand,
  formMonth,
} from '../../helpers';
import { DEMAND_MUTATION, UPLOAD_MUTATION } from '../../graphql/mutation';
import { DEMAND_QUERY } from '../../graphql/queries';

export const DataForm = ({ form, subject, topic }) => {
  const { data } = useContext(UserContext);
  const [files, setFiles] = useState([]);
  const initialValues = {
    nom: '',
    prenom: '',
    email: '',
    fonction: '',
    RRF: '',
    RRF_facture: '',
    numero_facture: '',
    adresse: '',
    codepostal: '',
    ville: '',
    telephone: '',
    chassis: '',
    SMS: '',
    periode_facturation: '',
    immatriculation: '',
    numero_commande: '',
    motif_annulation: '',
    date_envoi: '',
    heure_envoi: '',
    date_fermeture: '',
    text_libre: '',
    dataUser: {
      nom: data.firstname,
      prenom: data.lastname,
      telephone: data.phone,
      // email: data && data.email,
      email: '',
      codeRRF: data.rrf,
      codeDR: data.code_dr,
    },
  };
  const { setModal } = useModal();
  const { data: demandList, loading, refetch } = useQuery(DEMAND_QUERY);
  const [createUpload] = useMutation(UPLOAD_MUTATION);
  const [createDemand] = useMutation(DEMAND_MUTATION, {
    refetchQueries: [{ query: DEMAND_QUERY }],
  });
  const demandNumber = !loading && demandList.demandes
  && demandList.demandes.length > 0
  && demandList.demandes[0].NumeroDemande
    ? Number(demandList.demandes[0].NumeroDemande.split('–')[1]) + 1
    : 1;
  const sendRequest = async (values) => {
    const data = await handleDemand(
      subject,
      topic,
      demandNumber,
      createDemand,
      createUpload,
      files,
      values
    );
    try {
      await sendEmail(data);
      setModal(
        <span>
          <p>{`Votre demande n° ${data.NumeroDemande} a bien été envoyée.`}</p>
          <p>{'Nous vous répondrons sous 48 h.'}</p>
          <p>{'Merci d\'avoir contacté Renault Scope.'}</p>
        </span>
      );
    } catch (e) {
      console.log(e);
    }
  };
  const validationSchemaa = validationSchema(form);
  const formRef = useRef({});
  useEffect(() => {
    if (!isEmpty(formRef) && !isNull(formRef.current)) {
      formRef.current.resetForm();
      setFiles([]);
    }
    refetch();
  }, [form]);
  return (
    <>
      <span className="section-data__label">
        Complétez votre demande dans le(s) champ(s) ci-dessous.
      </span>
      <Formik
        innerRef={formRef}
        initialValues={initialValues}
        validationSchema={validationSchemaa}
        onSubmit={(values) => sendRequest(values)}
        render={({
          values, errors, touched, setFieldValue,
        }) => (
          <Form>
            {
              form.nom && (
                <Input
                  name="nom"
                  type="text"
                  value={values.nom}
                  label="Nom"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'nom')}
                />
              )
            }
            {
              form.prenom && (
                <Input
                  name="prenom"
                  type="text"
                  value={values.prenom}
                  label="Prénom"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'prenom')}
                />
              )
            }
            {
              form.email && (
                <Input
                  name="email"
                  type="text"
                  value={values.email}
                  label="Email"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'email')}
                />
              )
            }
            {
              form.fonction && (
                <Input
                  name="fonction"
                  type="text"
                  value={values.fonction}
                  label="Fonction"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'fonction')}
                />
              )
            }
            {
              form.RRF && (
                <Input
                  name="RRF"
                  type="text"
                  value={values.RRF}
                  maxLength="8"
                  label="Code RRF"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'RRF')}
                />
              )
            }
            {
              form.adresse && (
                <Input
                  name="adresse"
                  type="text"
                  value={values.adresse}
                  label="Adresse"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'adresse')}
                />
              )
            }
            {
              form.code_postal && (
                <Input
                  name="codepostal"
                  type="text"
                  maxLength="5"
                  value={values.codepostal}
                  label="Code Postal"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'codepostal')}
                />
              )
            }
            {
              form.ville && (
                <Input
                  name="ville"
                  type="text"
                  value={values.ville}
                  label="Ville"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'ville')}
                />
              )
            }
            {
              form.telephone && (
                <Input
                  name="telephone"
                  type="text"
                  maxLength="10"
                  value={values.telephone}
                  label="N° de Téléphone"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'telephone')}
                />
              )
            }
            {
              form.SMS && (
                <Input
                  name="SMS"
                  type="text"
                  maxLength="10"
                  value={values.SMS}
                  label="N° de SMS"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'SMS')}
                />
              )
            }
            {
              form.chassis && (
                <Input
                  name="chassis"
                  type="text"
                  value={values.chassis}
                  maxLength="17"
                  label="Numéro de châssis"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'chassis')}
                />
              )
            }
            {
              form.immatriculation && (
                <Input
                  name="immatriculation"
                  type="text"
                  maxLength="9"
                  value={values.immatriculation}
                  label="Numéro d’immatriculation"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'immatriculation')}
                />
              )
            }
            {
              form.numero_commande && (
                <Input
                  name="numero_commande"
                  type="text"
                  value={values.numero_commande}
                  maxLength="15"
                  label="Numéro de commande"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'numero_commande')}
                />
              )
            }

            {
              form.motif_annulation && (
                <div className="row">
                  <TextArea
                    name="motif_annulation"
                    rows="8"
                    value={values.motif_annulation}
                    label="Motif d'annulation"
                    errors={errors}
                    touched={touched}
                    style={getStyles(errors, touched, 'motif_annulation')}
                  />
                </div>
              )
            }
            {
              form.date_fermeture && (
                <Input
                  name="date_fermeture"
                  type="date"
                  max="9999-12-31"
                  value={values.date_fermeture}
                  label="Date de fermeture"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'date_fermeture')}
                />
              )
            }
            {
              form.numero_facture && (
                <Input
                  name="numero_facture"
                  type="text"
                  maxLength="8"
                  value={values.numero_facture}
                  label="Numéro de facture"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'numero_facture')}
                />
              )
            }
            {
              form.RRF_facture && (
                <Input
                  name="RRF_facture"
                  type="text"
                  value={values.RRF_facture}
                  maxLength="8"
                  label="RRF facture"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'RRF_facture')}
                />
              )
            }
            {
              form.date_envoi && (
                <>
                  <Input
                    name="date_envoi"
                    type="date"
                    value={values.date_envoi}
                    label="Date de l’envoi souhaité"
                    errors={errors}
                    touched={touched}
                    style={getStyles(errors, touched, 'date_envoi')}
                  />
                  <Input
                    name="heure_envoi"
                    type="time"
                    value={values.heure_envoi}
                    label="Heure de l’envoi souhaité"
                    errors={errors}
                    touched={touched}
                    style={getStyles(errors, touched, 'heure_envoi')}
                  />
                </>
              )
            }
            {
              form.periode_facturation && (
                <Input
                  name="periode_facturation"
                  type="month"
                  value={values.periode_facturation}
                  label="Période de facturation "
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'periode_facturation')}
                />
              )
            }
            { console.log('🚀 ~ file: dataForm.jsx ~ line 378 ~ DataForm ~ values.periode_facturation', values.periode_facturation)}
            <Input name="test" type="hidden" />
            {form.text_libre && (
              <div className="row">
                <TextArea
                  name="text_libre"
                  rows="8"
                  value={values.text_libre}
                  label="Compléments d'information"
                  errors={errors}
                  touched={touched}
                  style={getStyles(errors, touched, 'text_libre')}
                />
              </div>
            )}
            <div className="row">
            </div>
            <div className="file-wrapper">
              <FileInput files={files} setFiles={setFiles} />
            </div>
            <div className="row">
              <span> Complétez ou modifiez vos coordonnées de contact : </span>
            </div>
            <UserInformation
              values={values.dataUser}
              setFieldValue={setFieldValue}
              errors={errors}
              touched={touched}
              style={getStyles(errors, touched, 'dataUser')}
            />
            <div className="button-content">
              <button
                type="submit"
                className="btn-send"
              >

                Envoyer
              </button>
            </div>
          </Form>
        )}
      />
    </>
  );
};
