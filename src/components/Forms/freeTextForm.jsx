/* eslint-disable camelcase */
import React, {
  useContext, useState, useEffect, useRef,
} from 'react';
import { Formik, Form } from 'formik';
import { isEmpty, isNull } from 'lodash';
import { useMutation, useQuery } from '@apollo/react-hooks';
import {
  FileInput, UserInformation, TextArea, useModal,
} from '../commons';
import {
  getStyles,
  validationSchema,
  UserContext,
  sendEmail,
  handleDemand,
} from '../../helpers';
import { DEMAND_MUTATION, UPLOAD_MUTATION } from '../../graphql/mutation';
import { DEMAND_QUERY } from '../../graphql/queries';

export const FreeTextForm = ({ form, subject, topic }) => {
  const { data } = useContext(UserContext);
  const [files, setFiles] = useState([]);
  const initialValues = {
    text_libre: '',
    dataUser: {
      nom: data.firstname,
      prenom: data.lastname,
      telephone: data.phone,
      // email: data && data.email,
      email: '',
      codeRRF: data.rrf,
      codeDR: data.code_dr,
    },
  };
  const { setModal } = useModal();
  const { data: demandList, loading } = useQuery(DEMAND_QUERY);
  const [createUpload] = useMutation(UPLOAD_MUTATION);
  const [createDemand] = useMutation(DEMAND_MUTATION, {
    refetchQueries: [{ query: DEMAND_QUERY }],
  });
  const demandNumber = !loading && demandList.demandes
  && demandList.demandes.length > 0
  && demandList.demandes[0].NumeroDemande
    ? Number(demandList.demandes[0].NumeroDemande.split('–')[1]) + 1
    : 1;
  const sendRequest = async (values) => {
    const data = await handleDemand(
      subject,
      topic,
      demandNumber,
      createDemand,
      createUpload,
      files,
      values
    );

    await sendEmail(data);
    setModal(
      <span>
        <p>{`Votre demande n° ${data.NumeroDemande} a bien été envoyée.`}</p>
        <p>{'Nous vous répondrons sous 48 h.'}</p>
        <p>{'Merci d\'avoir contacté Renault Scope.'}</p>
      </span>
    );
  };
  const formRef = useRef({});
  useEffect(() => {
    if (!isEmpty(formRef) && !isNull(formRef.current)) {
      formRef.current.resetForm();
      setFiles([]);
    }
  }, [form]);
  const validationSchemaa = validationSchema(form);
  return (
    <>
      <span className="section-data__label">
        Complétez votre demande dans le(s) champ(s) ci-dessous.
      </span>
      <Formik
        innerRef={formRef}
        initialValues={initialValues}
        validationSchema={validationSchemaa}
        onSubmit={(values) => {
          sendRequest(values);
        }}
        render={({
          values, errors, touched, setFieldValue,
        }) => (
          <Form>
            <div className="row">
              <TextArea
                name="text_libre"
                rows="10"
                value={values.text_libre}
                label=""
                errors={errors}
                touched={touched}
                style={getStyles(errors, touched, 'text_libre')}
              />

            </div>
            <div className="row">
            </div>

            <div className="file-wrapper">
              <FileInput files={files} setFiles={setFiles} />
            </div>
            <div className="row">
              <span> Complétez ou modifiez vos coordonnées de contact : </span>
            </div>
            <UserInformation
              values={values.dataUser}
              setFieldValue={setFieldValue}
              errors={errors}
              touched={touched}
              style={getStyles(errors, touched, 'dataUser')}
            />
            <div className="button-content">
              <button
                type="submit"
                className="btn-send"
              >
                Envoyer
              </button>
            </div>
          </Form>
        )}
      />
    </>
  );
};
