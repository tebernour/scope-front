import React from 'react';
import { Document, Page } from 'react-pdf';

export const PDFViewer = (props) => {
  const { pdf } = props;
  return (
    <Document
      file={pdf}
      options={{ workerSrc: '/pdf.worker.js' }}
      onClick={() => { window.open(pdf); }}
    >
      <Page pageNumber={1} />
    </Document>
  );
};
