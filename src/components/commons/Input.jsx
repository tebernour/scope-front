import React from 'react';
import { useField } from 'formik';
export const Input = ({
  hideError, label, style, ...props
}) => {
  const [field, meta] = useField(props);

  return (
    <div className="form-group">
      <label htmlFor={label} className="section-data__label">{label}</label>
      <input style={style} {...field} {...props} />
      {meta.touched && meta.error && !hideError ? (
        <span className="error">{meta.error}</span>
      ) : null}
    </div>

  );
};
