import styled from 'styled-components';

const ModalWrapper = styled.div`
  .modal {
  --spacing: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100%;
  overflow: auto;
  background-color: rgba(0, 0, 0, 0.5);
}

.modal__close {
  display: block;
  position: fixed;
  background: rgba(255, 255, 255, 0.5);
  border: none;
  top: 0;
  left: 0;
  width: 32px;
  height: 32px;
}

.modal__inner {
  position: relative;
  width: calc(var(--spacing) * 36);
  width: 380px;
  padding: calc(var(--spacing) * 3) calc(var(--spacing) * 1.5);
  border-radius: 5px;
  border: 1px solid rgba(0, 0, 0, 0.1);
  background-color: #ffffff;
  box-shadow: rgba(9, 30, 66, 0.25) 0px 20px 32px -8px;
  text-align: center;
}

.modal__close-btn {
  position: absolute;
  top: 0;
  right: 10px;
  padding: 0;
  background: transparent;
  border: none;
  cursor: pointer;
  width: unset;
  &:focus {
    outline: unset;
  }
}

.modal__close-btn svg {
  width: var(--spacing);
  height: var(--spacing);
  fill: currentColor;
  opacity: 0.5;
}

.modal__close-btn:hover svg {
  opacity: 1;
}
  
  `;
export default ModalWrapper;
