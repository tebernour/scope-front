import React, { useState } from 'react';
import Dropzone from 'react-dropzone';
import { map, indexOf } from 'lodash';
import InputFileWrapper from './InputFileWrapper';

export const FileInput = ({ files, setFiles }) => {
  const [errors, setErrors] = useState('');
  const handleDrop = (acceptedFiles) => {
    if (acceptedFiles.length === 0) {
      setErrors('Vous pouvez joindre de 1 à 5 fichiers, taille maximum autorisée par fichier : 10 Mo');
    } else if (files.length < 5 || (files.length + acceptedFiles.length) === 5) {
      setFiles(acceptedFiles.concat(files));
      setErrors('');
    }
    if (files.length >= 5 || (files.length + acceptedFiles.length) > 5) {
      setErrors('Vous pouvez joindre de 1 à 5 fichiers, taille maximum autorisée par fichier : 10 Mo');
    }
  };
  const removeFile = (file) => () => {
    const newFiles = [...files];
    newFiles.splice(indexOf(newFiles, file), 1);
    setErrors('');
    setFiles(newFiles);
  };

  return (
    <InputFileWrapper>
      <span className="error">{errors}</span>
      <div className="file-wrapper">
        <div className="k-widget k-upload k-header">
          <div className="k-dropzone">
            <Dropzone
              files={files}
              onDrop={(acceptedFiles) => handleDrop(acceptedFiles)}
              maxFiles={5}
              maxSize={10240000}
            >
              {({
                getRootProps, getInputProps,
              }) => (

                <div {...getRootProps({ className: 'k-button k-upload-button' })}>
                  <input
                    {...getInputProps()}
                  />
                  <span>JOINDRE UN FICHIER </span>
                  <span>
                    Vous pouvez joindre de 1 à 5 fichiers,
                    taille maximum autorisée par fichier : 10 Mo
                  </span>
                </div>
              )}
            </Dropzone>
            <div>
              <ul className="k-upload-files k-reset">
                {map(files, (file, index) => (
                  <li className="k-file k-file-success" key={index}>
                    <span>{file.name}</span>
                    <i className="k-i-x" onClick={removeFile(file)} />
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </InputFileWrapper>
  );
};
