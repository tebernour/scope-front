import React from 'react';
import { useField } from 'formik';

export const TextArea = ({
  label, style, rows, ...props
}) => {
  const [field, meta] = useField(props);

  return (
    <div className="form-group">
      <label htmlFor={label}>{label}</label>
      <textarea style={style} rows={rows} {...field} {...props} />
      {meta.touched && meta.error ? (
        <span className="error">{meta.error}</span>
      ) : null}
    </div>

  );
};
