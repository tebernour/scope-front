/* eslint-disable quotes */
import React, { useState } from 'react';
import { split } from 'lodash';
import { Input } from '.';
import { getStyles } from '../../helpers';

export const UserInformation = ({
  values, errors, touched, setFieldValue,
}) => {
  const [state, setState] = useState(
    {
      prenom: values.prenom,
      nom: values.nom,
      telephone: values.telephone,
      codeRRF: values.codeRRF,
      email: values.email,
      codeDR: values.codeDR,
    }
  );
  const handelChange = (event) => {
    setState({
      ...state,
      [split(event.target.name, 'dataUser.')[1]]: event.target.value,
    });
  };
  const maxLengthCheck = (object) => {
    const number = object.target;
    if (number.value.length > number.maxLength) {
      number.value = number.value.slice(0, number.maxLength);
    }
  };

  return (
    <>
      <Input
        name={`dataUser.nom`}
        type="text"
        value={state.nom}
        label="Nom"
        onChange={(e) => { handelChange(e); setFieldValue(`dataUser.nom`, e.target.value); }}
        errors={errors}
        touched={touched}
        style={getStyles(errors, touched, `dataUser.nom`)}
      />
      <Input
        name={`dataUser.prenom`}
        type="text"
        value={state.prenom}
        label="Prénom"
        onChange={(e) => { handelChange(e); setFieldValue(`dataUser.prenom`, e.target.value); }}
        errors={errors}
        touched={touched}
        style={getStyles(errors, touched, `dataUser.prenom`)}
      />
      <Input
        name={`dataUser.email`}
        type="text"
        value={state.email}
        label="Email"
        onChange={(e) => { handelChange(e); setFieldValue(`dataUser.email`, e.target.value); }}
        errors={errors}
        touched={touched}
        style={getStyles(errors, touched, `dataUser.email`)}
      />
      <Input
        name={`dataUser.telephone`}
        type="text"
        maxLength="10"
        value={state.telephone}
        label="Tél"
        onChange={(e) => { handelChange(e); setFieldValue(`dataUser.telephone`, e.target.value); }}
        errors={errors}
        touched={touched}
        style={getStyles(errors, touched, `dataUser.telephone`)}
      />
      <Input
        name={`dataUser.codeRRF`}
        type="text"
        value={state.codeRRF}
        maxLength="8"
        label="Code RRF"
        onChange={(e) => { handelChange(e); setFieldValue(`dataUser.codeRRF`, e.target.value); }}
        errors={errors}
        touched={touched}
        style={getStyles(errors, touched, `dataUser.codeRRF`)}
      />
      <Input
        name={`dataUser.codeDR`}
        type="number"
        value={state.codeDR}
        label="DR"
        maxLength="2"
        onInput={maxLengthCheck}
        onChange={(e) => { handelChange(e); setFieldValue(`dataUser.codeDR`, e.target.value); }}
        errors={errors}
        touched={touched}
        style={getStyles(errors, touched, `dataUser.codeDR`)}
      />
    </>
  );
};
