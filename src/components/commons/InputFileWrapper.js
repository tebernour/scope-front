import styled from 'styled-components';
import colors from '../../assets/styles/colors';

const InputFileWrapper = styled.div`
  .k-upload {
    border-width: 1px;
    box-sizing: border-box;
    outline: 0;
    font-family: inherit;
    font-size: 14px;
    line-height: 1.42857143;
    position: relative;
    display: block;
    -webkit-touch-callout: none;
    -webkit-tap-highlight-color: transparent;
  }
  .k-upload .k-upload-button {
    width: 300px;
    font-weight: bold; 
    top: 8em;
    margin-left: 20px;
    text-align: center;
  }
  .k-dropzone {
    width: 600px;
  }
  .k-upload .k-dropzone {
    border-width: 0;
    display: flex;
    align-items: center;
    justify-content: space-between;
    position: absolute;
    top: 0px;
    background-color: ${colors.lightGray};
    span {
      text-align: left;
      &:nth-child(3) {
        position: absolute;
        bottom: -29px;
        left: 0px;
      }
    }
  }
  .k-upload .k-upload-files {
    margin: 0;
    padding: 0;
    border-width: 1px 0 0;
    border-color: inherit;
    list-style: none;
    position: absolute;
  }
  .k-upload .k-upload-files .k-file {
    padding: 0;
    border-width: 0 0 1px;
    border-color: inherit;
    outline: 0;
    display: flex;
    -ms-flex-align: start;
    align-items: flex-start;
    position: relative;
  }

  .k-upload .k-upload-files .k-upload-action {
    border-width: 0;
    color: inherit;
    background: 0 0;
    box-shadow: none;
  }
  .k-upload .k-progress {
    height: 2px;
    position: absolute;
    bottom: 0;
    left: 0;
  }
  .k-button {
    background-clip: padding-box;
    border-radius: 2px;
    padding: 4px 8px;
    box-sizing: border-box;
    border-width: 2px;
    border-style: solid;
    font-size: 14px;
    line-height: 1.42857143;
    font-family: inherit;
    text-align: center;
    text-decoration: none;
    white-space: nowrap;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    vertical-align: middle;
    user-select: none;
    cursor: pointer;
    outline: 0;
    -webkit-appearance: none;
    position: relative;
  }
  .k-button .k-ie11 {
    background-clip: border-box;
  }
  .k-upload-button::after {
    position: absolute;
    font-weight: bold;
    left: 90%;
    content: '>';
  }
  .k-button-icon {
    width: calc(1.42857143em + 10px);
    height : calc(1.42857143em + 10px);
    padding: 1px;
  }
 
  .selected-files {
    display: inline-block;
    width: 100%;
    background: yellow;
  }
.k-upload .k-upload-files .k-file-size {
    line-height: 0;
    display: inline-block;
    width: 100%;
    background: yellow;
}
.k-upload .k-upload-files .k-file-size,
.k-upload .k-upload-files .k-file-extension-wrapper{
  display: none;
}
ul {
  display: inline-block;
  li {
    text-align: unset;
    font-weight: bold;
    max-width: 500px;
    overflow-wrap: break-word;
      span {
        text-align: right;
        font-weight: bold;
        margin-right: 20px;
        overflow-wrap: break-word;
        display: inline-block;
        font-size: large;
    }
    .k-i-x::before {
      content: "X";
  }
  i {
    font-style: unset;
    width: 10px;
  }
  }
}
`;

export default InputFileWrapper;
