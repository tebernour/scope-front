import React from 'react';
import HeaderWrapper from './headerWrapper';
import logo from '../../assets/images/LOGO_Renault_Scope.png';

const Header = ({
  className,
}) => (
  <div className={className}>
    <div className="header__top-section">
      <span>aller à l'administration</span>
    </div>
    <div className="header__content">
      <div>
        <span> One Vision</span>
        <br />
        <span> Le portail de la publicité réseau Renault</span>
      </div>
      <div>
        <img src={logo} alt="logo" />
      </div>
    </div>
    <div className="header__bottom-section"></div>
  </div>

);

export default HeaderWrapper(Header);
