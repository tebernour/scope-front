import styled from 'styled-components';
import colors from '../../assets/styles/colors';
export default (Component) => styled(Component)`
    .header__top-section {
        display: inline-block;
        font-family: 'RenaultLife_Regular';
        width: 100%;
        background: ${colors.lightGray} 0% 0% no-repeat padding-box;
        opacity: 1;
        height: 29px;
        padding-top: 6px;
        span {
            color: ${colors.darkGray};
            text-transform: uppercase;
            font-weight: bold;
            margin-left: 137px;
        }
    }
    .header__content {
        
        font-family: 'RenaultLife_Bold';
        display: flex;
        justify-content: space-between;
        max-width: 80%;
        margin: 20px auto;
        color: ${colors.dark};
        span {
            &:first-child {
                font-size: 27px;
                text-transform: uppercase;
            }
            &:nth-child(2) {
                font-size: 20px;
            }
        }
        img {
            width: 316px;
            height: 50px;
            float: right;
        }
    }
    .header__bottom-section {
        width: 100%;
        background: ${colors.lightGray} 0% 0% no-repeat padding-box;
        opacity: 1;
        height: 37px;
    }

`;
