/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import TextTest from '../TextTest';

describe('test text', () => {
  it('should match snapshot', () => {
    const tree = renderer.create(
      <TextTest text="test updated" />,
    );
    expect(tree).toMatchSnapshot();
  });
  it('testText', () => {
    const wrapper = shallow(<TextTest text="test success" />);
    expect(wrapper.text()).toEqual('test success');
  });
});
