import React from 'react';

const TextTest = ({ text }) => <p className="text">{text}</p>;

export default TextTest;