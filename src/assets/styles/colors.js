const colors = {
  dark: '#000000',
  veryDarkGray: '#4A4A4A',
  darkGray: '#545454',
  gray: '#DCDCDC',
  lightGray: '#ececec',
  yellow: '#FFCC33',
  white: '#FFFFFF',
  red: '#FF0000',
};
export default colors;
