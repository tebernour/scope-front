import { createGlobalStyle } from 'styled-components';
import colors from './colors';

const GlobalStyle = createGlobalStyle`

html{
    width:100%;
    height:100%;
}
*{
    margin: 0;
}

.contact--page {
    max-width: 80%;
    font-family: 'RenaultLife_Regular';
    margin: 2.25em auto;
}

input{
  -webkit-appearance: none;
  width: 100%;
  height: 2.5em;
  background: transparent;
  padding: 0;
  margin-top: 5px;
  border: 2px solid ${colors.lightGray};
}

input[type="month"]:focus::before,
input[type="month"]:active::before {
  content: "";
  width: 0%;
}

h1,h2,h3,h4,h5,h6,h7{
  padding: 0;
  margin:0;
}
h1 {
    font-family: 'RenaultLife_Bold' !important;
    font-size: 2.5em;
}
h2 {
    font-weight: 400;
    font-size: 1.5em;
}
h3, ul {
    font-weight: 400;
    font-size: 0.875em;
}
ul {
    margin: 0;
}
span {
    font-size: 0.875em;
    font-weight: 400; 
}
form {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
}
select {
    font-family: 'RenaultLife_Regular';
    width: 100%;
    height: 2.875em;
    line-height: 1.25em;
    background: ${colors.lightGray} 0% 0% no-repeat padding-box;
    opacity: 1;
    padding: 0 1.25em;
    font-size: 0.875em;
    -webkit-appearance: none;
    -moz-appearance: none;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none; 
    -ms-word-break: normal;
    word-break: normal;
    border: none;
}
select:disabled {
    color:${colors.dark};
}
button {
    width: 100%;
    font-family: 'RenaultLife_Bold';
    height: 2.875em;
    line-height: 1.25em;
    color: ${colors.dark};
    font-size: 0.875em;
    background: transparent;
    border-color:${colors.dark};
    text-transform: uppercase;
    font-weight: bold;
}
button:disabled {
    cursor: not-allowed; 
}
button.btn-send {
    background: ${colors.yellow};
    border: none
}
pre {
    display: inline-block;
    width: 100%;
    font-family: 'RenaultLife_Regular';
    max-width: 600px;
    white-space: break-spaces;

}
span.error {
    color:  ${colors.red};
    font-size: 1em;
}
.form-group {
    margin-bottom: 20px;
    flex-basis: 18.5625em;
}

.row{
    width: 100%;
}
.row input[type="textarea"]{
        width: 100%;
        height: 10em;
}
.row span {
        line-height: 40px;
    }
.file-wrapper {
    text-align: center;
    width: 100%;
    min-height: 12em;
    vertical-align: middle;
    display: table-cell;
    position: relative;
    background: ${colors.lightGray};
  }
  .file-wrapper input {
    position: absolute;
    top: 0;
    right: 0;
    cursor: pointer;
    opacity: 0.0;
    filter: alpha(opacity=0);
  }
   textarea {
    resize: none;
    outline: none;
    width: 100%;
    -webkit-appearance: none;
    background: transparent;
    padding: 0;
    border: 1px solid ${colors.dark};
    }
    label {
    color: ${colors.dark};
    font-size: 14px;
    text-align: left;
    font-weight: 400;
    margin-top: 17px;
    }
    .form-group input[type="radio"] {
        height: 12px;
        width: 12px;
        margin-left: 5px;
        margin-right: 5px;
        appearance: none;
        margin-top: -1px;
        display: table-cell;
        vertical-align:middle;
    }
    .form-group input[type='radio']:after {
        width: 10px;
        height: 10px;
        border-radius: 10px;
        top: 0px;
        left: 0px;
        position: relative;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid ${colors.white};
    }

    .form-group input[type='radio']:checked:after {
        width: 10px;
        height: 10px;
        border-radius: 10px;
        top: 0px;
        left: 0px;
        position: relative;
        background-color: ${colors.dark};
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid ${colors.white};
        background-color: ${colors.white};
    }
    `;
export default GlobalStyle;
