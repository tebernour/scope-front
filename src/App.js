import React from 'react';
import Routes from './routes';
import Layout from './pages/Layout';
import { ModalProvider } from './components/commons';

function App() {
  return (
    <>
      <Layout>
        <div className="contact--page">
          <ModalProvider>
            <Routes />
          </ModalProvider>
        </div>
      </Layout>
    </>
  );
}

export default App;
