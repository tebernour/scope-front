#!/bin/bash

set -e

export ENV=$1

#TODO: remove CI assignment to prevent warning from being deployed
#CI="" yarn build
export IMAGE_NAME=${BITBUCKET_REPO_SLUG}
export IMAGE_TAG=${ENV}-${BITBUCKET_COMMIT}


yes y | gcloud auth configure-docker

docker build  --build-arg ENV=${ENV} -t eu.gcr.io/scope-292312/${IMAGE_NAME}:${IMAGE_TAG} .
docker push eu.gcr.io/scope-292312/${IMAGE_NAME}:${IMAGE_TAG}
