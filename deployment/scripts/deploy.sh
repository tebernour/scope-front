#!/bin/bash

set -e

# connecto to cluster
export ENV=${1}

case  ${ENV} in
"dev")
export GCLOUD_PROJECT=scope-292312
export GCLOUD_ZONE=europe-west1-c
export KUBERNETES_CLUSTER=scope-recette
;;
"qa")
export GCLOUD_PROJECT=scope-test-292710
export GCLOUD_ZONE=europe-west1-c
export KUBERNETES_CLUSTER=scope-test-cluster
;;
*)
  echo "Environment not supported!"
  exit 1;
;;
esac




gcloud container clusters get-credentials ${KUBERNETES_CLUSTER}  --zone ${GCLOUD_ZONE} --project ${GCLOUD_PROJECT}

export IMAGE_NAME=scope-front
export IMAGE_TAG=${ENV}-${BITBUCKET_COMMIT}

  echo "Test deploying version: ${IMAGE_TAG}"
  helm upgrade --install ${IMAGE_NAME} deployment/${IMAGE_NAME} --values deployment/${IMAGE_NAME}/${ENV}-values.yaml --set image.tag=${IMAGE_TAG} --dry-run=True
  echo "Deploying version: ${IMAGE_TAG}"
  helm upgrade --install ${IMAGE_NAME} deployment/${IMAGE_NAME} --values deployment/${IMAGE_NAME}/${ENV}-values.yaml --set image.tag=${IMAGE_TAG}


