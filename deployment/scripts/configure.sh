#!/bin/bash

set -e

export ENV=${1}

case  ${ENV} in
"dev")
export GCLOUD_PROJECT=scope
export GCLOUD_ZONE=europe-west1-c
export GCLOUD_API_KEYFILE=${GCLOUD_API_KEYFILE_DEV}
;;
"qa")
export GCLOUD_PROJECT=scope-test
export GCLOUD_ZONE=europe-west1-c
export GCLOUD_API_KEYFILE=${GCLOUD_API_KEYFILE_QA}
;;
*)
  echo "Environment not supported!"
  exit 1;
;;
esac

kubectl version --client

# Helm
HELM_VERSION=v3.3.4

curl -O https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 

 bash ./get-helm-3 

helm version --client

# Authenticating with the service account key file
echo ${GCLOUD_API_KEYFILE} | base64 --decode --ignore-garbage > ~/cicd-cluster-dev.json
gcloud auth activate-service-account --key-file ~/cicd-cluster-dev.json
# Linking to the Google Cloud project
gcloud config set project ${GCLOUD_PROJECT}
gcloud config set compute/zone ${GCLOUD_ZONE}
