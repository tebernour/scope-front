# SCOPE-Front !

=========

Front repository will consume scope_backoffice(STRAPI)

### How to use -- Developer edition
```bash
git clone https://bitbucket.org/oyez/scope_front.git
cd scope_front
yarn
cp .env.example .env
```

### Run Scope 
```bash
yarn start
```

### Run Test Jest 
```bash
yarn test 
```

### Run coverage test
```bash
yarn test:cov
```

### Run Test update snapshot 
```bash
yarn test:update
```
# Git-Projects
